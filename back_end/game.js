var express = require ('express');
var router  = express.Router ();

// Supporting Scripts
const Board = require('./board.js');

router.get('/getGodId', function (req, res) {
  let godId = {id:123};
  console.log ("GoD Id Requested");
  // CORS, Open API
  res.set('Access-Control-Allow-Origin', '*');
  res.write(JSON.stringify(godId));
  res.end();
});

router.get ('/new', function (req, res){
  // Create a new Board
  let gameBoard = new Board(123);
  // TODO: Move to middleware
  if (req.body.id > 0){
    // gameBoard exists, load it
    gameBoard.get(id);
  } else {
    // Initialize default size of 5
    // TODO: Read this as an input???
    gameBoard.build(5);
  }
  // CORS, Open API
  res.set('Access-Control-Allow-Origin', '*');
  res.write (gameBoard.print());
  res.end();
  // Create a new Board
  // Create Id
  // Add Board State to DB with Id as key
  // Return state of Board in JSON
});

router.post ('/clear', function (req, res){
  // CORS, Open API
  res.set('Access-Control-Allow-Origin', '*');

  console.log(req.body);

  if (req.body.id == undefined) {
    res.write("Invalid input");
    res.end();
    return;
  }

  // Create a new Board
  let gameBoard = new Board(req.body.id);

  // TODO: Move to middleware
  // Retrieve the Board in play
  gameBoard.get();
  console.log (gameBoard.print());
  let retVal = {val : gameBoard.clearBox(req.body.row, req.body.col) };
  res.write(JSON.stringify(retVal));
  // Clear the selected Box
//  gameBoard.clear(req.body.row, req.body.col);

//  console.log(gameBoard.clearBox(req.body.row, req.body.col));

  // TODO: Move to middleware
  // Print the Board
  //res.write (gameBoard.print());

  res.end();
});

router.get('/clear', function (req, res){
  console.log ("GET clear");
  res.set('Access-Control-Allow-Origin', '*');
  res.write("Dude");
  res.end();
  // Extract Board from DB By Id
  // Clear the selected box
  // Update the Board State in the DB
  // Return the state of Board in JSON
});

module.exports = router;
