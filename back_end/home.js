var express = require ('express');
var router  = express.Router ();

// Supporting Scripts
const Board = require('./board.js');

router.get ('/', function (req, res){
  // Create a new Board
  let gameBoard = new Board(123);

  // TODO: Move to middleware
  if (req.body.id > 0){
    // gameBoard exists, load it
    gameBoard.get(id);
  } else {
    // Initialize default size of 5
    gameBoard.build(5);
  }

  // TODO: Move to middleware
  // Create Card for Board
  // Print the Board
  res.write (gameBoard.print());

  // New Game Button
  // Resume Game Option
  // Form to add Game Id
  // Button to submit
  res.end ();
});

router.post ('/clear', function (req, res){
  // Create a new Board
  let gameBoard = new Board(req.body.id);

  // TODO: Move to middleware
  // Retrieve the Board in play
  gameBoard.get();

  // Clear the selected Box
  gameBoard.clear(req.body.row, req.body.col);

  // TODO: Move to middleware
  // Print the Board
  res.write (gameBoard.print());

  res.end();
});

router.get ('/resume', function (req, res){
  res.write ("Resume");
  res.end();
});



module.exports = router;
