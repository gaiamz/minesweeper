// Express App
var express = require ('express');
var app     = express ();
var cors    = require ('cors');

// Require Routes
var game  = require ('./game.js');
//var score = require ('./score.js');

const port = process.env.PORT;

app.use (express.json({type:['application/json', 'text/plain']}));
app.use (express.urlencoded ({ extended: true }));

// Use Routes for pages
app.use ('/game', game);
//app.use ('/score', score);

// Start Express App
app.listen (8080, localhost => {
  console.log ('Success'); //`Server running at http://${hostname}:${port}/`);
});
