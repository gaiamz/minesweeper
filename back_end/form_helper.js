var helper = {};

helper.builder = function (id, action){
  return {
    formId: id,
    formAction: action,
    formColor: "light-grey",
    formMethod: "post",
    setId: function(id){
      this.formId = id;
      return this;
    },
    setAction: function(action){
      this.formAction = action;
      return this;
    },
    setColor: function(color){
      this.formColor = color;
      return this;
    },
    setMethod: function(method){
      this.formMethod = method;
      return this;
    },
    addInput: function(label, name, type, value=""){
      if (this.inputs){
        this.inputs.push({"type":type, "label":label, "name":name, "value":value});
      } else {
        this.inputs = [{"type":type, "label":label, "name":name, "value":value}];
      }
      return this;
    },
    addSubmitInput: function (value){
      this.submitButton = value;
      return this;
    },
    build: function(){
      let form = "<div class='w3-container w3-"+this.formColor+"'>";
      form += "<form action='"+this.formAction+"' method='"+this.formMethod+"' id='"+this.formId+"'></form>";
      if (this.inputs) {
        for (let i=0; i < this.inputs.length; i++){
          if (this.inputs[i].label) {
            form += "<label>"+this.inputs[i].label+":</label>";
          }
          form += "<input class='w3-input w3-animate-input w3-border' style='width:50%' form='" + this.formId + "' name=\""+this.inputs[i].name+"\" type='" + this.inputs[i].type + "' value='" + this.inputs[i].value + "'>";
        }
      }
      if (this.submitButton) {
        form += "<br><input class='w3-button w3-blue' form='" + this.formId + "' type='Submit' value='" + this.submitButton + "'>";
      }
      return form + "</div>";
    }
  };
};

module.exports = helper;
