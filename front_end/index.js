// let baseAddr = "localhost:8080/"
let baseAddr = "https://sweeper.christhall.us/api"

function createGameBoard() {
  let gameBoard = document.getElementById("board");
  // Set the Game Id of the Board
  let data = fetch("https://sweeper.christhall.us/api/game/getGodId")
//  let data = fetch("http://localhost:8080/game/getGodId")
    .then(response =>{
      // Extract the JSON object
      return response.json();
    }).then(data =>{
      console.log(data);
      // Update the board with the JSON data
      gameBoard.setAttribute("gameId", data.id);
    });

  // Create the grid of Boxes
  for (let i = 0; i < 5; i++) {
    let row = document.createElement("div");
    row.setAttribute("class", "row");
    for (let j = 0; j < 5; j++) {
      let box = document.createElement("sweeper-box");
      box.setAttribute("row", i);
      box.setAttribute("col", j);
//      box.setAttribute("val", "");
      // Pass the Box element when clicked
      box.addEventListener("click", function() {clickBox(box);});
      row.appendChild(box);
    }
    gameBoard.appendChild(row);
  }
}

function createBoard(board) {
  // Extract the Board Div
  let gameBoard = document.getElementById("board");
  // Loop over the board and create a grid of boxes
  for (let i = 0; i < board.board.length; i++) {
    // Create a DIV for each Row
    let row = document.createElement("div");
    row.setAttribute("row", i);
    row.setAttribute("class","row");
    row.setAttribute("id", "row"+i);
    for (let j = 0; j < board.board.length; j++) {
      // Create a Div for each Box in the Row
      let box = document.createElement("div");
      box.setAttribute("gameId", board.id);
      box.setAttribute("row", board.board[i][j].row);
      box.setAttribute("col", board.board[i][j].col);
      box.setAttribute("class","box");
      box.setAttribute("id","box"+i+j);
      box.innerHTML = board.board[i][j].value;
      // Pass the Box element when clicked
      box.addEventListener("click", function() {clickBox(box);});
      // Attach the box to the row
      row.appendChild(box);
    }
    // Attache the row to the Board
    gameBoard.appendChild(row);
  }
}

function newGame() {
  // Get the Game Board from the backend
  let fetchAddr = baseAddr + '/game/new';
  console.log (fetchAddr);
  let gameBoard = fetch("https://sweeper.christhall.us/api/game/new")
    .then(response =>{
      // Extract the JSON object
      return response.json();
    }).then(data =>{
      console.log (data);
      // Update the board with the JSON data
      createBoard(data);
    });
}

function updateBoard(board) {
  for (let i = 0; i < board.board.length; i++) {
    for (let j = 0; j < board.board[i].length; j++) {
      let box = document.getElementById("box"+i+j);
      box.innerHTML = board.board[i][j].value;
    }
  }
}

function clickBox(box) {
  let board = document.getElementById("board");
  let gameBoard = fetch('https://sweeper.christhall.us/api/game/clear', {
//  let gameBoard = fetch('http://localhost:8080/game/clear', {
      method: 'POST',
      body: JSON.stringify({
        'id':board.getAttribute("gameId"),
        'row':box.getAttribute("row"),
        'col':box.getAttribute("col")
      })
    }).then(response => {
      return response.json();
    }).then(data => {
      console.log (data);
      box.setAttribute("val", data.val);
//      updateBoard(data);
    });
}

// TODO: Update Board Id attribute on html load
function getBoardId() {
  fetch('localhost:8080/game/getId')
    .then(response => {
      return response.json();
    }).then(data => {
      board.setAttribute("id", data);
    });
}

// TODO: 

//let newGameButton = document.getElementById("newGame");
//newGameButton.addEventListener("click", function() {newGame();});

createGameBoard();