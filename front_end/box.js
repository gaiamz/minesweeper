class SweeperBox extends HTMLElement {

  static get observedAttributes() {
    return ['val'];
  }

  constructor() {
    super();

    // Create a shadow root
    const shadowRoot = this.attachShadow({mode: 'open'});

    // Apply external sytle to the shadow DOM
    const linkElem = document.createElement('link');
    linkElem.setAttribute('rel', 'stylesheet');
    linkElem.setAttribute('href', 'box.css');
    shadowRoot.appendChild(linkElem);

    // Create the outer shell
    const wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'box');
    // Set the Row Attribute
    let rowVal = '0';
    if (this.hasAttribute('row')) {
      rowVal = this.getAttribute('row');
    }
    wrapper.setAttribute('row', rowVal);
    // Set the Col Attribute
    let colVal = '0';
    if (this.hasAttribute('col')) {
      colVal = this.getAttribute('col');
    }
    wrapper.setAttribute('col', colVal);

    // Create the inner Value
    const text = document.createElement('h1');
    text.setAttribute('id', 'value');
    // Set the text
    if (this.hasAttribute('val')) {
      text.innerHTML = this.getAttribute('val');
    }

    wrapper.appendChild(text);

    this.shadowRoot.appendChild(wrapper);
  }

  attributeChangedCallback(name, oldVal, newVal) {
    console.log ("Updating " + name);
    console.log (this.shadowRoot);
    this.shadowRoot.querySelector('h1').innerHTML = newVal;
  }
}

customElements.define("sweeper-box", SweeperBox);